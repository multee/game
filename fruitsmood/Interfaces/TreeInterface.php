<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 18:00
 */
namespace FruitsMood\Interfaces;
interface TreeInterface {
    public function generateFruits();
    public function grow($stepHours);
}
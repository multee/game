<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 17:52
 */
namespace FruitsMood\Interfaces;
interface AnimalInterface {
    public function eat($fruit, $part);
    public function getLikeFruits();
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 18:00
 */

namespace FruitsMood\Interfaces;
interface ConnectionInterface
{
    public function __construct($config);

    public function load($number);

    public function save($dataLevel);

    public function log($params);

    public function findLevelByNumber($number);

    public function checkPassword($number, $password);

    public function checkLevel($number);

    public function remove($number, $password);
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 13:26
 */
namespace FruitsMood\Factories;

use FruitsMood\AbstractFactories\AbstractAnimalFactory;
use FruitsMood\Classes\Human;
use FruitsMood\Classes\Tree;

class HumansFactory extends AbstractAnimalFactory
{
    private $trees;
    public function __construct($trees)
    {
        $this->trees = $trees;
    }

    public function makeAnimal($likeFruits, $mood, $id)
    {
        return new Human($likeFruits, $mood, $id);
    }

    public function makeHumans($humans)
    {
        $makedHumans = [];
        foreach ($humans as $human) {
            $makedHumans[$human['id']] = $this->makeAnimal($human['likeFruits'], $human['mood'], $human['id']);
        }
        return $makedHumans;
    }

    public function makeRandomHuman($id) {
        $trees = $this->trees;
        $likeFruits = [];
        /** @var Tree $tree */
        foreach ($trees as $tree) {
            $likeFruits[$tree->getType()] = rand(-10, 10) / 10;
        }
        return $this->makeAnimal($likeFruits, rand(0, 300), $id);
    }

    public function makeAnimals($count)
    {
        $humans = [];
        for ($i=0; $i < $count; $i++) {
            $humans[] = $this->makeRandomHuman($i);
        }
        return $humans;
    }
}
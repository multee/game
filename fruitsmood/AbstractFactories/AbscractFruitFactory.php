<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 13:10
 */
namespace FruitsMood\AbstractFactories;
abstract class AbscractFruitFactory
{
    abstract function makeFruit($tree);
    abstract function makeRandomFruits($tree, $count);
}
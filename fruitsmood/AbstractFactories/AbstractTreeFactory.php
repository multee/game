<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 11:51
 */
namespace FruitsMood\AbstractFactories;
abstract class AbstractTreeFactory // по сути просто интерфейс
{
    abstract function makeTree($type, $sweetness, $id, $fruits);
    abstract function makeTrees($trees);
    abstract function makeRandomTrees($trees);
}
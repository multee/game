<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 13:10
 */
namespace FruitsMood\AbstractFactories;
abstract class AbstractAnimalFactory
{
    abstract function makeAnimal($likeFruits, $mood, $id);
    abstract function makeAnimals($count);
}
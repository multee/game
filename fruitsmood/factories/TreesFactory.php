<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 12:22
 */

namespace FruitsMood\Factories;

use FruitsMood\AbstractFactories\AbstractTreeFactory;
use FruitsMood\Classes\Tree;

class TreesFactory extends AbstractTreeFactory
{

    private $trees;

    public function __construct()
    {
        $this->trees = [
            'Apple',
            'Limon',
            'Orange'
        ];
    }

    public function makeTree($type = null, $sweetness = null, $id = null, $fruits = null)
    {
        if (!$type) {
            $treePosition = rand(0, count($this->trees) - 1);
            $type = $this->trees[$treePosition];
        }

        $functionName = 'make' . $type;
        return $this->$functionName($sweetness, $id, $fruits);
    }

    /**
     * @param $trees
     * @return array
     */
    public function makeTrees($trees)
    {
        $treesArray = [];
        foreach ($trees as $tree) {
            $treeObject = $this->makeTree($tree['type'], $tree['sweetness'], $tree['id'], $tree['fruits']);
            $treesArray[$tree['id']] = $treeObject;
        }
        return $treesArray;
    }

    public function makeRandomTrees($count)
    {
        $trees = [];
        for ($i = 0; $i < $count; $i++) {
            $trees[] = $this->makeTree();
        }
        return $trees;
    }

    /**
     * @param $sweetnessMinMax
     * @param $stateComplete
     */
    public function getStartSweetness($sweetnessMinMax, $stateComplete)
    {
        $sweetnessRate = ($sweetnessMinMax[1] - $sweetnessMinMax[0]) / 100;
        $startSweetness = $sweetnessRate * $stateComplete + $sweetnessMinMax[0];
        return $startSweetness;
    }

    public function makeApple($sweetness = null, $id = null, $fruits = null)
    {
        if (!$sweetness) {
            $sweetness = $this->getStartSweetness([80, 100], rand(1, 100));
        }
        return new Tree($this->trees[0], $sweetness, 6, $id, $fruits);
    }

    public function makeLimon($sweetness = null, $id = null, $fruits = null)
    {
        if (!$sweetness) {
            $sweetness = $this->getStartSweetness([0, 30], rand(1, 100));
        }
        return new Tree($this->trees[1], $sweetness, 12, $id, $fruits);
    }

    public function makeOrange($sweetness = null, $id = null, $fruits = null)
    {
        if (!$sweetness) {
            $sweetness = $this->getStartSweetness([60, 80], rand(1, 100));
        }
        return new Tree($this->trees[2], $sweetness, 16, $id, $fruits);
    }
}
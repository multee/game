<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 07.06.18
 * Time: 12:22
 */
namespace FruitsMood\Factories;

use FruitsMood\AbstractFactories\AbscractFruitFactory;
use FruitsMood\Classes\Fruit;
use FruitsMood\Classes\Tree;

class FruitsFactory extends AbscractFruitFactory {

    /**
     * @param Tree $tree
     * @return Fruit
     */
    public function makeFruit($tree, $stateComplete = null, $stateDamage = null, $position = null)
    {
        if (is_null($stateComplete)) {
            $stateComplete = rand(0, 100);
        }

        if (is_null($stateDamage)) {
            $stateDamage = 0;
            if ($stateComplete == 100) {
                $stateDamage = rand(0, 100);
            }
        }

        if (is_null($position)) {
            $randomPosition = $this->getRandomPosition();
            $center = [40, 50];
            $position = [
                $randomPosition[0] + $center[0],
                $randomPosition[1] + $center[1]
            ];
        }
        $options = [
            'type' => $tree->getType(),
            'sweetness' => $tree->getSweetness(),
            'growTime' => $tree->getGrowTime(),
            'stateComplete' => $stateComplete,
            'stateDamage' => $stateDamage,
            'position' => $position
        ];
        return new Fruit($options);
    }


    /**
     * @param Tree $tree
     * @param $count
     * @return Fruit[]
     */
    public function makeRandomFruits($tree, $count)
    {
        $fruits = [];
        for ($i = 0; $i < $count; $i++) {
            $fruits[] = $this->makeFruit($tree);
        }
        return $fruits;
    }

    public function getRandomPosition($radius = 15) {
        $x = rand(-$radius, $radius);
        $yMax = sqrt(pow($radius, 2) - pow($x, 2));
        $y = rand(-$yMax, $yMax);
        return [$x, $y];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 06.06.18
 * Time: 17:47
 */

namespace FruitsMood\Classes;

class Fruit
{
    private $type;
    private $stateComplete;
    private $stateDamage;
    private $sweetness;
    private $growTime;
    private $damageTime;
    private $position;

    public function __construct($options)
    {
        $this->type = $options['type'];
        $this->sweetness = $options['sweetness'];
        $this->growTime = $options['growTime'];
        $this->damageTime = $options['growTime'];
        $this->stateComplete = $options['stateComplete'];
        $this->stateDamage = $options['stateDamage'];
        $this->position = $options['position'];
    }

    public function grow($stepHours)
    {
        $stateComplete = $this->getStateComplete();
        $stateComplete += ((100 / $this->growTime) * $stepHours) ;
        $this->setStateComplete($stateComplete);

        if ($stateComplete >= 100) {
            $stateDamage = $this->getStateDamage();
            $stateDamage += ((100 / $this->damageTime) * $stepHours) ;
            $this->setStateDamage($stateDamage);
        }
        return $this;
    }

    public function isDown()
    {
        // Фрукты падают
        return $this->getStateComplete() >= 100;
    }

    /**
     * @return mixed
     */
    public function getStateComplete()
    {
        return $this->stateComplete;
    }

    /**
     * @return mixed
     */
    public function getStateDamage()
    {
        return $this->stateDamage;
    }

    /**
     * @return mixed
     */
    public function getSweetness()
    {
        return $this->sweetness;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $sweetness
     */
    public function setSweetness($sweetness): void
    {
        $this->sweetness = $sweetness;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    private function setStateComplete($stateComplete) {
        $stateComplete = $stateComplete <= 100 ? $stateComplete : 100;
        $this->stateComplete = $stateComplete;
    }
    private function setStateDamage($stateDamage) {
        $stateDamage = $stateDamage <= 100 ? $stateDamage : 100;
        $this->stateDamage = $stateDamage;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 06.06.18
 * Time: 17:16
 */
namespace FruitsMood\Classes;

use FruitsMood\Factories\FruitsFactory;

class Tree
{
    private $id;
    private $type;
    private $sweetness;
    private $fruits;
    private $growTime;
    public function __construct($type=null, $sweetness=null, $growTime=null, $id=null, $fruits=null)
    {
        $this->id = $id;
        $this->type = $type;
        $this->growTime = $growTime;
        if ($sweetness) {
            $this->sweetness = $sweetness;
        }

        if ($fruits !== null) {
            $this->setFruitsFromArray($fruits);
        }
        else {
            if ($sweetness) {
                $this->generateFruits();
            }
        }
    }

    public function generateFruits() {
        $fruitFactory = new FruitsFactory();
        $fruits = $fruitFactory->makeRandomFruits($this, rand(0, 3));
        $this->setFruits($fruits);
    }

    public function addFruits() {
        $fruitFactory = new FruitsFactory();
        $fruits = $fruitFactory->makeRandomFruits($this, rand(0, 3));
        $this->setFruits(array_merge($this->getFruits(), $fruits));
    }

    public function grow($stepHours) {
        // Фрукты растут и падают и удаляются
        $fruits = $this->getFruits();
        $newFruits = [];
        foreach ($fruits as $fruit) {
            $newFruit = $fruit->grow($stepHours);
            if ($newFruit->getStateDamage() < 100) {
                $newFruits[] = $newFruit;
            }
        }
        $this->setFruits($newFruits);
        return $this;
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Fruit[]
     */
    public function getFruits()
    {
        return $this->fruits;
    }

    /**
     * @param mixed $fruits
     */
    private function setFruits($fruits)
    {
        $this->fruits = $fruits;
    }

    /**
     * @param mixed $fruits
     */
    private function setFruitsFromArray($fruits)
    {
        $fruitsArray = [];
        $fruitFactory = new FruitsFactory();
        foreach ($fruits as $fruitId => $fruit) {
            $fruitsArray[$fruitId] = $fruitFactory->makeFruit($this, $fruit['stateComplete'], $fruit['stateDamage'], $fruit['position']);
        }
        $this->fruits = $fruitsArray;
    }

    /**
     * @return mixed
     */
    public function getGrowTime()
    {
        return $this->growTime;
    }

    /**
     * @return mixed
     */
    public function getSweetness()
    {
        return $this->sweetness;
    }

    public function removeFruitById($id) {
        $fruits = $this->getFruits();
        $newFruits = [];
        foreach ($fruits as $fruitId => $fruit) {
            if ($fruitId != $id) {
                $newFruits[] = $fruit;
            }
        }
        $this->setFruits($newFruits);
        return $this;
    }
}
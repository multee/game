<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 06.06.18
 * Time: 17:46
 */

namespace FruitsMood\Classes;

use FruitsMood\Interfaces\AnimalInterface;

class Human implements AnimalInterface
{
    private $likeFruits;
    private $mood;
    private $id;

    public function __construct($likeFruits, $mood = null, $id = null)
    {
        $this->id = $id;
        $this->likeFruits = $likeFruits;
        $this->setMood($mood);
    }

    /**
     * @param Fruit $fruit
     * @param number $part
     */
    public function eat($fruit, $part = 100)
    {
        $type = $fruit->getType();
        $likeFruits = $this->likeFruits;
        $mood = $this->getMood();
        $mood += ($likeFruits[$type] * $fruit->getSweetness() * $fruit->getStateComplete() / $part);
        $this->setMood($mood);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLikeFruits()
    {
        return $this->likeFruits;
    }

    /**
     * @return mixed
     */
    public function getMood()
    {
        return $this->mood;
    }

    /**
     * @param mixed $mood
     */
    private function setMood($mood): void
    {
        $this->mood = $mood;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }
}
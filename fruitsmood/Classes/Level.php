<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 06.06.18
 * Time: 17:45
 */

namespace FruitsMood\Classes;

use FruitsMood\Factories\HumansFactory;
use FruitsMood\Factories\TreesFactory;
use Psr\Log\LoggerInterface;

class Level

// use DI
{
    private $items;
    private $password;
    private $connection;
    private $id;

    /**
     * Level constructor.
     * @param bool $number
     * @param bool $password
     * @param Logger|null $logger
     */
    public function __construct($number = false, $password = false, Connection $connection, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        if ($password) {
            $this->setPassword($password);
            if ($number) {
                if ($this->logger) {
                    $this->logger->info('загружаем уровень');
                }
                $this->load($number);
            } else {
                if ($this->logger) {
                    $this->logger->info('создаем уровень');
                }
                $this->create();
                $level = $this->save(true);
                if ($level) {
                    $number = $level['id'];
                }
            }
            $this->id = $number;
        }
    }

    /**
     * @param $password
     */
    public function create()
    {
        $password = $this->getPassword();
        if ($password) {
            if ($this->logger) {
                $this->logger->info('создаем деревья');
            }

            $treesFactory = new TreesFactory();
            $items['trees'] = $treesFactory->makeRandomTrees(6);
            /** @var Tree $tree */
            foreach ($items['trees'] as $tree) {
                if ($this->logger) {
                    $this->logger->info('Дерево - ' . $tree->getType());
                }
                /** @var Fruit $fruit */
                foreach ($tree->getFruits() as $fruit) {
                    if ($this->logger) {
                        $this->logger->info('Плод ' . $fruit->getType() . '  зрелость - ' . $fruit->getStateComplete() . ', Сладость - ' . $fruit->getSweetness());
                    }
                }
            }
            if ($this->logger) {
                $this->logger->info('создаем людей');
            }

            $humansFactory = new HumansFactory($items['trees']);
            $items['humans'] = $humansFactory->makeAnimals(3);
            $this->items = $items;
        }
    }

    public function load($number)
    {
        $level = $this->connection->load($number);
        if ($this->logger) {
            $this->logger->info('создаем деревья');
        }

        $treesFactory = new TreesFactory();
        $items['trees'] = $treesFactory->makeTrees($level['trees']);
        $humansFactory = new HumansFactory($items['trees']);
        $items['humans'] = $humansFactory->makeHumans($level['humans']);
        $this->items = $items;
    }


    public function save($isNew=false)
    {
        if ($this->logger) {
            $this->logger->info('сохраняем уровень');
        }
        if ($this->getItems()) {
            if ($this->connection) {
                $items = $this->getItemsArray(true);
                if (!$isNew) {
                    $items['id'] = $this->getId();
                    $oldLevel = $this->connection->load($items['id']);
                    $items['password'] = $oldLevel['password'];
                }
                return $this->connection->save($items);
            }
        }
        return false;
    }

    /**
     * @return Tree[]
     */
    public function getTrees()
    {
        return $this->items['trees'];
    }

    /**
     * @return mixed
     */
    public function getHumans()
    {
        return $this->items['humans'];
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getItemsArray($forSave = false)
    {
        $items = $this->getItems();
        $treesAraay = [];
        /** @var Tree $tree */
        foreach ($items['trees'] as $treeId => $tree) {
            $fruits = $tree->getFruits();
            $fruitsArray = [];
            /** @var Fruit $fruit */
            foreach ($fruits as $fruitId => $fruit) {
                $fruitsArray[$fruitId] = [
                    'id' => $fruitId,
                    'type' => $fruit->getType(),
                    'sweetness' => $fruit->getSweetness(),
                    'stateComplete' => $fruit->getStateComplete(),
                    'stateDamage' => $fruit->getStateDamage(),
                    'position' => $fruit->getPosition(),
                    'isDown' => $fruit->isDown()
                ];
            }
            $treesAraay[$treeId] = [
                'id' => $treeId,
                'type' => $tree->getType(),
                'sweetness' => $tree->getSweetness(),
                'fruits' => $fruitsArray
            ];
        }
        /** @var Human $human */
        foreach ($items['humans'] as $humanId => $human) {
            $humansArray[$humanId] = [
                'id' => $humanId,
                'likeFruits' => $human->getLikeFruits(),
                'mood' => $human->getMood()
            ];
        }
        $itemsForSave = [
            'trees' => $treesAraay,
            'humans' => $humansArray
        ];
        if ($forSave) {
            $itemsForSave['password'] = $this->getPassword();
        }

        return $itemsForSave;
    }

    /**
     * @param $password
     */
    private function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }
    public function getId () {
        return (int)$this->id;
    }

    /**
     * @param mixed $items
     */
    private function setItems($items): void
    {
        $this->items = $items;
    }

    public function play ($hours) {
        $trees = $this->getTrees();
        $newTrees = [];
        /** @var Tree $tree */
        foreach ($trees as $tree) {
            $newTrees[] = $tree->grow($hours);
            $tree->addFruits();
        }
        $items = $this->getItems();
        $items['id'] = $this->getId();
        $items['trees'] = $newTrees;
        $this->setItems($items);
        $this->save();
    }

    public function humansEat () {
        $humans = $this->getHumans();
        $newHumans = [];
        /** @var Human $human */
        foreach ($humans as $human) {
            $fruitForEat = $this->humanChooseFruit($human);
            if (!is_null($fruitForEat)) {
                $messsage = 'Человек #' . $human->getId() . ' удовольствие '. $human->getMood();
                $newHumans[] = $human->eat($fruitForEat);
                $messsage .= ' съел фрукт типа ' . $fruitForEat->getType() . ', сладость ' . $fruitForEat->getSweetness();
                $messsage .= ' спелость ' . $fruitForEat->getStateComplete();
                $messsage .= ' испорченность ' . $fruitForEat->getStateDamage();
                $messsage .= ' удовольствие стало ' . $human->getMood();
                $this->logger->info([
                    'type' => 'human',
                    'params' => [
                        'levelId' => $this->getId(),
                        'humanId' => $human->getId(),
                        'message' => $messsage
                    ]
                ]);
            }
            else {
                $newHumans[] = $human;
            }
        }


        $items = $this->getItems();
        $items['id'] = $this->getId();
        $items['humans'] = $newHumans;
        $this->setItems($items);
        $this->save();
    }

    private function humanChooseFruit($human) {
        $trees = $this->getTrees();
        $newTrees = [];
        $treePosition = rand(0, count($trees));
        /** @var Fruit $fruitForEat */
        $fruitForEat = null;
        /** @var Tree $tree */
        foreach ($trees as $tree) {

            $fruits = $tree->getFruits();
            if ($tree->getId() == $treePosition) {
                $fruitPosition = rand(0, count($fruits));
                foreach ($fruits as $fruitId => $fruit) {
                    if ($fruitPosition == $fruitId) {
                        $fruitForEat = $fruit;
                        $tree = $tree->removeFruitById($fruitId);
                    }
                }
            }
            $newTrees[] = $tree;
        }
        $items = $this->getItems();
        $items['id'] = $this->getId();
        $items['trees'] = $newTrees;
        $this->setItems($items);
        return $fruitForEat;
    }
}
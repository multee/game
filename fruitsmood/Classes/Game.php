<?php

namespace FruitsMood\Classes;

use FruitsMood\Interfaces\ConnectionInterface;
use Psr\Log\LoggerInterface;
use FruitsMood\Classes\Human;
use FruitsMood\Classes\Tree;
use FruitsMood\Classes\Level;

class Game
{
    private $level;
    private $connection;
    private $logger;

    function __construct(LoggerInterface $logger, ConnectionInterface $connection, $number = null, $password = null)
    {
        $this->connection = $connection;
        $this->logger = $logger;
        
        if (!is_null($password)) {
            if ($number) {
                if ($this->connection->findLevelByNumber($number)) {
                    $level = new Level($number, $password, $this->connection, $logger);
                } else {
                    $level = null;
                }

            } else {
                if ($this->logger) {
                    $this->logger->info("создаем игру");
                }
                $level = new Level(false, $password, $this->connection, $logger);
            }
            /** @var Level $this ->level */
            $this->level = $level;
        }
    }

    private function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }
}
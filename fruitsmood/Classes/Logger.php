<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 08.06.18
 * Time: 14:04
 */

namespace FruitsMood\Classes;


use FruitsMood\Interfaces\ConnectionInterface;
use Psr\Log\LoggerInterface;

class Logger implements LoggerInterface
{
    protected $connection;
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function alert($message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
    public function critical($message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
    public function debug($message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
    public function emergency($message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
    public function error($message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
    public function info($message, array $context = array())
    {
        if (isset($context['type']) && isset($context['params'])) {
            switch ($context['type']) {
                case 'human':
                    $this->connection->log($context['params']);
                    break;
            }
        }

     // echo "<p>$message</p>";
    }
    public function log($level, $message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
    public function notice($message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
    public function warning($message, array $context = array())
    {
     // echo "<p>$message</p>";
    }
}
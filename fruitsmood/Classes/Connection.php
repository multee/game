<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 06.06.18
 * Time: 18:22
 */
namespace FruitsMood\Classes;

use FruitsMood\Interfaces\ConnectionInterface;

class Connection implements ConnectionInterface
{
    private $config;
    private $store;
    public function __construct($config=null)
    {
        $this->config = $config;
        $this->store = $this->getStoreAsArray();
    }

    public function load ($number) {
        $filename = './store/level/level' . $number . '.json';
        $level = json_decode(file_get_contents($filename), true);
        return $level;
    }

    public function save ($dataLevel) {
        // с одной стороны, судя по названию, метод типизирован, но принимает просто массив
        $level = $dataLevel;
        if (!isset($level['id'])) {
            $level['id'] = $this->getNextLevelId();
        }
        if ($level['id'] !== false) {
            $filename = './store/level/level' . $level['id'] . '.json';
            file_put_contents($filename, json_encode($level));
            return $level;
        }
        return false;
    }

    private function getNextLevelId() { // зачем публичный
        $store = $this->getStoreAsArray();
        if (isset($store['level'])) {
            return count($store['level']) + 1;
        }
        return false;
    }

    /**
     * @return array
     */
    private function getStoreAsArray() {// зачем публичный
        $dir = './store';
        return $this->dirToArray($dir);
    }

    /**
     * @param $dir
     * @return array
     */
    private function dirToArray($dir) {// зачем публичный
        $result = array();

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value)
        {
            if (!in_array($value,array(".","..")))
            {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
                {
                    $result[$value] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value);
                }
                else
                {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    public function findLevelByNumber($number) { // испльзуется в Game но нет в интерфейсе
        $store = $this->getStoreAsArray();
        if (isset($store['level'])) {
            if (count($store['level']) > 0) {
                return in_array('level' . $number . '.json', $store['level']);
            }
        }
        return null;
    }
    public function checkPassword ($number, $password) { // используется в index.php но нет в интерфейсе
        $store = $this->getStoreAsArray();
        if (isset($store['level'])) {
            $level = $this->load($number);
            if (!$level) {
                return false;
            }
            if (isset($level['password'])) {
                return $level['password'] === $password;
            }
        }
        return false;
    }

    public function checkLevel ($number) { // используется в index.php но нет в интерфейсе
        $store = $this->getStoreAsArray();
        if (isset($store['level'])) {
            $level = $this->load($number);
            if (!$level) {
                return false;
            }
            return true;
        }
        return false;
    }

    public function log($params) { //зачем это
        $levelId = $params['levelId'];
        $humanId = $params['humanId'];
        $message = $params['message'];
        $message .= "\n";
        $filename = './store/human/human' . $humanId . '-level' . $levelId . '.txt';
        file_put_contents($filename, $message, FILE_APPEND);
    }

    public function getHumanActions($humanId, $levelId) {
        $filename = './store/human/human' . $humanId . '-level' . $levelId . '.txt';
        return file($filename);
    }

    public function remove ($number, $password) { // используется в index.php но нет в интерфейсе
        $filename = './store/level/level' . $number . '.json';
        file_put_contents($filename, json_encode([]));
        return true;
    }
}
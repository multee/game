<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 06.06.18
 * Time: 19:29
 */

use FruitsMood\Classes\Connection;
use FruitsMood\Classes\Game;
use FruitsMood\Classes\Level;
use FruitsMood\Classes\Logger;
use FruitsMood\Interfaces\ConnectionInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

require 'vendor/autoload.php';

$app = new \Slim\App;
// Get container
$container = $app->getContainer();
$container['createLogger'] = function ($contaiter) {
    return function (ConnectionInterface $connection, LoggerInterface $loggerClass) {
        return new Logger($connection);
    };
};

$container['createConnection'] = function ($contaiter) {
    return function ($config = null) {
        return new Connection($config);
    };
};

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('front', [
        'cache' => false
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));

    return $view;
};
$app->get('/', function ($request, $response) {
    return $this->view->render($response, 'index.html');
})->setName('index');

$app->get('/human/{levelId}/{humanId}', function ($request, $response, array $args) {
    $levelId = $args['levelId'];
    $humanId = $args['humanId'];

    /** @var ConnectionInterface $connection */
    $connection = $this->createConnection();
    $messages = $connection->getHumanActions($humanId, $levelId);
    return $this->view->render($response, 'human.html', [
        'levelId' => $levelId,
        'humanId' => $humanId,
        'messages' => $messages
    ]);
})->setName('human');

$app->post('/game/{number}/remove', function (Request $request, Response $response, array $args) {
    $number = $args['number'];
    /** @var ConnectionInterface $connection */
    $connection = $this->createConnection();
    /** @var \Psr\Log\LoggerInterface $logger */
    $logger = $this->createLoader($connection);

    $body = $request->getParsedBody();
    if ($number) {
        $password = $body['password'];
        if ($password) {
            if (!$connection->checkLevel($number)) {
                $logger->error('no game');
                $response = $response->withJson(['status' => 'ERROR', 'message' => 'no game']);
                return $response;
            }
            if (isset($password)) {
                if (!$connection->checkPassword($number, $password)) {
                    $logger->error('bad password');
                    $response = $response->withJson(['status' => 'ERROR', 'message' => 'bad password']);
                    return $response;
                }
            }
            if ($connection->remove($number, $password)) {
                return $response = $response->withJson(['status' => 'SUCCESS', 'data' => ['id' => $number]]);
            }
            return $response = $response->withJson(['status' => 'ERROR', 'message' => 'some error']);

        }
    }
});

$app->post('/game/{number}', function (Request $request, Response $response, array $args) {
    $body = $request->getParsedBody();
    $number = $args['number'];
    if ($this->has('createConnection')) {
        /** @var ConnectionInterface $connection */
        $connection = $this->get('createConnection')();
    }
    if ($this->has('createLogger')) {
        /** @var \Psr\Log\LoggerInterface $logger */
        $logger = $this->get('createLogger')($connection);
    }
    if ($number == 'new') {
        $password = $body['password'];
        if ($password) {
            $game = new Game($logger, $connection, false, $password);

            $level = $game->getLevel();
            $items = $level->getItemsArray();
            $items['id'] = $level->getId();
            $response = $response->withJson(['status' => 'SUCCESS', 'data' => $items]);
        } else {
            $logger->error('no password');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no password']);
            return $response;
        }

        return $response;
    }
    if ($number === 'run') {
        if (!isset($body['id'])) {
            $logger->error('no game');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no game']);
            return $response;
        }
        $id = $body['id'];

        if (!$connection->checkLevel($id)) {
            $logger->error('no game');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no game']);
        }

        $game = new Game($logger, $connection, $id, true);

        $hours = rand(1, 7);
        /** @var Level $level */
        $level = $game->getLevel();
        $level->play($hours);
        $items = $level->getItemsArray();
        $items['id'] = $level->getId();
        $response = $response->withJson(['status' => 'SUCCESS', 'data' => $items]);
        return $response;
    }
    if ($number === 'eat') {
        if (!isset($body['id'])) {
            $logger->error('no game');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no game']);
            return $response;
        }
        $id = $body['id'];

        if (!$connection->checkLevel($id)) {
            $logger->error('no game');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no game']);
        }

        $game = new Game($logger, $connection, $id, true);

        /** @var Level $level */
        $level = $game->getLevel();
        $level->humansEat();
        $items = $level->getItemsArray();
        $items['id'] = $level->getId();
        $response = $response->withJson(['status' => 'SUCCESS', 'data' => $items]);
        return $response;
    }
    if ($number) {
        $password = $body['password'];
        if (!$connection->checkLevel($number)) {
            $logger->error('no game');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no game']);
            return $response;
        }
        if (isset($password)) {
            if (!$connection->checkPassword($number, $password)) {
                $logger->error('bad password');
                $response = $response->withJson(['status' => 'ERROR', 'message' => 'bad password']);
                return $response;
            }
        } else {
            $logger->error('no password');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no password']);
            return $response;
        }

        $game = new Game($logger, $connection, $number, $password);
        if ($game->getLevel()) {
            $level = $game->getLevel();
            $items = $level->getItemsArray();
            $items['id'] = $level->getId();
            $response = $response->withJson(['status' => 'SUCCESS', 'data' => $items]);
            return $response;
        } else {
            $logger->error('no game');
            $response = $response->withJson(['status' => 'ERROR', 'message' => 'no game']);
            return $response;
        }
    }
});
$app->run();
